package com.example.sqlite_student;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;

public class EditContact extends Activity {

    private static int RESULT_LOAD_IMAGE = 1;

    private ContactHandler handler;

    EditText et_name;
    EditText et_phone;
    EditText et_email;
    EditText et_address;
    ImageView iv_photograph;
    private byte[] photograph;
    private Bitmap bp;

    Bundle extras;

    @SuppressLint({"CutPasteId", "SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        extras = getIntent().getExtras();

        handler = new ContactHandler(getApplicationContext());


        et_name = findViewById(R.id.et_name);
        et_name.setText(extras.getString("name"));

        et_phone = findViewById(R.id.et_phone);
        et_phone.setText(extras.getString("phone"));

        et_email = findViewById(R.id.et_email);
        et_email.setText(extras.getString("email"));

        et_address = findViewById(R.id.et_address);
        et_address.setText(extras.getString("address"));
        byte[] b = extras.getByteArray("photograph");
        assert b != null;
        bp = BitmapFactory.decodeByteArray(b, 0, b.length);
        iv_photograph = findViewById(R.id.iv_user_photo);
        iv_photograph.setImageBitmap(bp);
        //Log.e("image",extras.getString("photograph"));

        Button btn_update = findViewById(R.id.btn_add);
        btn_update.setText("Update");
        btn_update.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                photograph = profileImage(bp);
                Contact contact = new Contact();
                contact.setID(extras.getInt("id")); // Update the data where id = extras.getInt("id").
                contact.setName(et_name.getText().toString());
                contact.setPhoneNumber(et_phone.getText().toString());
                contact.setEmail(et_email.getText().toString());
                contact.setPostalAddress(et_address.getText().toString());
                contact.setPhotograph(photograph);

                boolean updated = handler.editContact(contact); // calling ContactHandlermethod
                if(updated){
                    Intent intent = new Intent(EditContact.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "Contact data not updated. Please try again", Toast.LENGTH_LONG).show();
                }


            }
        });

        ImageView iv_user_photo = findViewById(R.id.iv_user_photo);
        iv_user_photo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, RESULT_LOAD_IMAGE);

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        ImageView iv_user_photo1 = findViewById(R.id.iv_user_photo);
        if(resultCode == RESULT_OK){
            Uri choosenImage = data.getData();
            if(choosenImage !=null){
                bp=decodeUri(choosenImage);
                iv_user_photo1.setImageBitmap(bp);
            }
        }
    }

    protected Bitmap decodeUri(Uri selectedImage) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage),
                    null, o);
            // The new size we want to scale to
            // final int REQUIRED_SIZE = size;
            // Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            Log.e("width",String.valueOf(width_tmp) );
            Log.e("height",String.valueOf(height_tmp) );
            int scale = 1;
            while (width_tmp / 2 >= 100
                    && height_tmp / 2 >= 100) {
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }
            Log.e("width",String.valueOf(width_tmp) );
            Log.e("height",String.valueOf(height_tmp) );
            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return
                    BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o2);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
    //Convert bitmap to bytes
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    private byte[] profileImage(Bitmap b){
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.PNG, 0, bos);
        return bos.toByteArray();
    }
}
