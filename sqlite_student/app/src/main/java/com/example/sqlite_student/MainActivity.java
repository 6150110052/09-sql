package com.example.sqlite_student;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ContactHandler handler;
    private List<Contact> contacts;
    ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        handler = new ContactHandler(this);
        contacts = handler.readAllContacts();
        lv = (ListView) findViewById(R.id.lv_contact_list);
        loadContactData();
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Intent intent = new Intent(MainActivity.this, ContactDetailActivity.class);
                intent.putExtra("id", contacts.get(position).getID());
                intent.putExtra("name", contacts.get(position).getName());
                intent.putExtra("phone", contacts.get(position).getPhoneNumber());
                intent.putExtra("email", contacts.get(position).getEmail());
                intent.putExtra("address", contacts.get(position).getPostalAddress());
                intent.putExtra("photograph", contacts.get(position).getPhotograph());
                startActivity(intent);
            }
        });
    }
    private void loadContactData(){
        // Code for loading contact list in ListView
        // Reading all contacts
        contacts = handler.readAllContacts();
        // Initialize Custom Adapter
        CustomAdapter adapter = new CustomAdapter(this, contacts);
        // Set Adapter to ListView
        lv.setAdapter(adapter);
        // See the log int LogCat
        for(Contact c : contacts){
            String record = "ID=" + c.getID() + " | Name=" + c.getName() + " | " +
                    c.getPhoneNumber();
            Log.d("Record",record);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add_record) {
            Intent add_mem = new Intent(this, AddNewContact.class);
            startActivity(add_mem);
        }
        return super.onOptionsItemSelected(item);
    }
}